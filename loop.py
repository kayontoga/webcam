import argparse
import pyfakewebcam
import numpy as np
import cv2
import time
import os
import sys

from mlhub.pkg import get_cmd_cwd
from PIL import Image

# Replace the webcam stream with a video loop from file.

# ----------------------------------------------------------------------
# Parse command line arguments
# ----------------------------------------------------------------------

option_parser = argparse.ArgumentParser(add_help=False)

option_parser.add_argument(
    'path',
    help='path to mp4 file')

args = option_parser.parse_args()
path = os.path.join(get_cmd_cwd(), args.path)

if not os.path.isfile(path):
    sys.exit(f'mlhub webcam loop: file not found "{path}"')

# ----------------------------------------------------------------------
# Camera devices.
# ----------------------------------------------------------------------

DUMCAM = 4  # /dev/video4 created with sudo modprobe v4l2loopback devices=1
DUMDEV = f'/dev/video{DUMCAM}'

# ----------------------------------------------------------------------
# Width and height of output screen.
# ----------------------------------------------------------------------

OUT_W = 1280
OUT_H = 720

# ----------------------------------------------------------------------
# Output dummy camera.
# ----------------------------------------------------------------------

try:
    camera = pyfakewebcam.FakeWebcam(DUMDEV, OUT_W, OUT_H)
except FileNotFoundError:
    sys.exit()

# ----------------------------------------------------------------------
# Video file as the source for the fake camera.
# ----------------------------------------------------------------------

vid = cv2.VideoCapture(path)
fps = vid.get(cv2.CAP_PROP_FPS)

try:

    while True:

        # Get video frames.
    
        ret, vid_frame = vid.read()

        # Restart the video if it has reached the last frame.
    
        if not ret:
            vid.set(cv2.CAP_PROP_POS_FRAMES, 0)
            continue

        # Change color format and convert to PIL images.
    
        vid_res = Image.fromarray(cv2.cvtColor(vid_frame, cv2.COLOR_BGR2RGB))

        # Set the video to the correct size for the webcam.
    
        vid_res = vid_res.resize((OUT_W, OUT_H))

        # Place the frame in the output buffer and keep same frames per second.
    
        camera.schedule_frame(np.array(vid_res))
        time.sleep(1/fps)

except KeyboardInterrupt:

    sys.exit(0)
