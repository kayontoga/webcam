#!/bin/sh

if ! (v4l2-ctl --list-devices | grep 'Dummy video') > /dev/null; then
  echo "Create a Dummy webcam (requires admin access)."
  sudo modprobe v4l2loopback devices=1
fi

