import pyfakewebcam
import numpy as np
import cv2
import sys

from PIL import Image

from utils import WEBCAM, DUMDEV, OUT_W, OUT_H

# Stream the real webcam thru the dummy webcam.

# ----------------------------------------------------------------------
# Output dummy camera.
# ----------------------------------------------------------------------

try:
    camera = pyfakewebcam.FakeWebcam(DUMDEV, OUT_W, OUT_H)
except FileNotFoundError:
    sys.exit()

# ----------------------------------------------------------------------
# Input camera.
# ----------------------------------------------------------------------

cap = cv2.VideoCapture(WEBCAM)

# Without the following two the video looks squashed.

cap.set(3, OUT_W)
cap.set(4, OUT_H)

try:
    while True:
        # Get camera frames.
        ret, cap_frame = cap.read()
        # Change color format and convert to PIL images.
        cap_res = Image.fromarray(cv2.cvtColor(cap_frame, cv2.COLOR_BGR2RGB))
        cap_res = cap_res.resize((OUT_W, OUT_H))
        # Place the frame in the output buffer.
        camera.schedule_frame(np.array(cap_res))
except KeyboardInterrupt:
    sys.exit(0)
