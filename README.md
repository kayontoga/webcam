Web Cam
========

This [MLHub](https://mlhub.ai) package provides command line tools
that utilise the
[pyfakewebcam](https://github.com/jremmons/pyfakewebcam) package to
create a webcam device that can then be used as a video source for
video conferencing tools like Zoom. The command line tools can loop a
mp4 video file into the new webcam, or take the live image from an
actual webcam and process it before feeding it into the new webcam to
provide different privacy options for any video broadcast. 

Visit the gitlab repository for more details:
<https://gitlab.com/kayontoga/webcam>

The Python code is based on an example provided by Alexander Mathews
of the Australian National University's Software Innovation Institute
and in turn based on the pyfakewebcam source repository.

Quick Start Command Line Examples
---------------------------------

```console
$ ml install gitlab:kayontoga/webcam
$ ml configure webcam
$ ml dummy webcam
$ ml gui webcam
```

Some useful commands:

```console
$ v4l2-ctl --list-devices               # List video devices.
$ sudo modprobe v4l2loopback devices=1  # Create dummy device.
$ vlc v4l2:///dev/video4                # View video signal.
$ sudo rmmod v4l2loopback               # Remove the dummy device.
```

See https://survivor.togaware.com/mlhub/webcam.html for further
documentation.

Usage
-----

After installing, the **configure** will create a new dummy webcam
device. You will need to do this after a reboot too. 

To identify the webcam devices, including the newly created dummy
device:

```console
$ v4l2-ctl --list-devices

Dummy video device (0x0000) (platform:v4l2loopback-000):
	/dev/video4

Integrated Camera: Integrated C (usb-0000:00:14.0-8):
	/dev/video0
	/dev/video1
	/dev/video2
	/dev/video3
```

By default this package assumes the real webcam is /dev/video0 and the
dummy webcam is /dev/video4. Eventually, the lowest numbered integrated
camera will be taken as the existing webcam and the highest numbered
dummy video will be taken as the default new webcam. Command line
options will allow these to be changed, but for now edit the files in
~/.mlhub/webcam.

To play your own video file into the dummy webcam as a continuous loop:

```console
$ ml loop webcam myvid.mp4
```

Then within a video conferencing application specify the webcam as the
dummy video device (/dev/video4).

A static image can also be shown:

```console
$ ml show webcam mypic.png
```

The actual web cam can be streamed via the dummy webcam:

```console
$ ml thru webcam
```

To transform the live video feed into a video showing the detected
edges:

```console
$ ml edge webcam
```

An embossed version of the live streaming video:

```console
$ ml emboss webcam
```

To convert the video stream into contours:

```console
$ ml contour webcam
```
