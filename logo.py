import argparse
import pyfakewebcam
import numpy as np
import cv2
import sys
import os

from mlhub.pkg import get_cmd_cwd
from PIL import Image

from utils import WEBCAM, DUMDEV, OUT_W, OUT_H

# Insert a resized image as a logo into the video strean.

# ----------------------------------------------------------------------
# Parse command line arguments
# ----------------------------------------------------------------------

option_parser = argparse.ArgumentParser(add_help=False)

option_parser.add_argument(
    'path',
    help='path to image file')

args = option_parser.parse_args()
path = os.path.join(get_cmd_cwd(), args.path)

if not os.path.isfile(path):
    sys.exit(f'mlhub webcam logo: file not found "{path}"')

# The offset was explored in an attempt to suit Zoom cropping the
# image though it is not clear what Zoom is acutally doing. When going
# full screen in Zoom the full image is displayed, though the default
# display when not full screen was clipping the image. Noted though
# that when in meeting with others the full screen is displayed.

LOGO_W = 100
LOGO_H = 100

LOGO_OFFSET = (OUT_W-LOGO_W, 0)

# ----------------------------------------------------------------------
# Output dummy camera.
# ----------------------------------------------------------------------

try:
    camera = pyfakewebcam.FakeWebcam(DUMDEV, OUT_W, OUT_H)
except FileNotFoundError:
    sys.exit()

# ----------------------------------------------------------------------
# Input camera.
# ----------------------------------------------------------------------

cap = cv2.VideoCapture(WEBCAM)

# Without the following two the video looks squashed.

cap.set(3, OUT_W)
cap.set(4, OUT_H)

logo = Image.open(path)
logo.thumbnail((LOGO_W, LOGO_H))

try:
    while True:
        # Get camera frames.
        ret, cap_frame = cap.read()
        # Change color format and convert to PIL images.
        cap_res = Image.fromarray(cv2.cvtColor(cap_frame, cv2.COLOR_BGR2RGB))
        cap_res = cap_res.resize((OUT_W, OUT_H))
        # Add static logo image
        cap_res.paste(logo, LOGO_OFFSET, logo)
        # Place the frame in the output buffer.
        camera.schedule_frame(np.array(cap_res))
except KeyboardInterrupt:
    cap.release()
    sys.exit(0)
